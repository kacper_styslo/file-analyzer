# -*- coding: utf-8 -*-
# !/usr/bin/env python3

# PSL
import re
from os import getuid, path, remove
from pathlib import Path
from pwd import getpwuid

# Third part
try:
    from findspark import init

    init(f"/home/{getpwuid(getuid())[0]}/spark-3.1.2-bin-hadoop2.7")
    from pyspark.sql import Row

except (ValueError, ModuleNotFoundError) as err:
    raise err

# Own
from _pyspark._operators import SPARK_STREAMING
from _pyspark.jobs.five_most_common_words_in_text import get_top_five_most_common_words_in_text


def __capture_text_file_to_analyze():
    return SPARK_STREAMING.textFileStream(path.join(Path(__file__).parent, "data", "to_analyze"))


def __create_row_from_file() -> Row:
    file_to_analyze = __capture_text_file_to_analyze()
    return file_to_analyze.flatMap(lambda line: re.split(" ", line.lower().strip()))


if __name__ == '__main__':
    __create_row_from_file().foreachRDD(get_top_five_most_common_words_in_text)
    SPARK_STREAMING.start()
    SPARK_STREAMING.awaitTermination()
    SPARK_STREAMING.stop(stopSparkContext=True, stopGraceFully=True)
