# file-analyzer

## Description
If you want to analyze text file just put this file in data\to_analyze directory.
Script will save output here: data\analyzed.
Don't restart script to analyze another file, you can add another text files all time because script will capture and analyze them.


## Setup
```
Windows: python run_dstream.py
Linux: python3 run_dstream.py
```


