# PSL
import os
from os import listdir, path, remove
from pathlib import Path
from typing import List, NoReturn

# Third part
from pyspark.sql import Row
from pyspark.sql.utils import AnalysisException

# Own
from _pyspark._operators import SPARK_SESSION


def get_top_five_most_common_words_in_text(text_row: Row) -> NoReturn:
    data_dir = path.join(Path(__file__).parent.parent.parent, "data")
    try:
        text_rdd = text_row.map(lambda word: Row(word=word))
        words_df = SPARK_SESSION.createDataFrame(text_rdd)
        words_df.createOrReplaceTempView("words")
        five_most_common_words = SPARK_SESSION.sql(
            "SELECT word, count(*) as ammount FROM words WHERE LENGTH(word) >= 2 GROUP BY word ORDER BY ammount DESC LIMIT 5")
        analyzed_file: str = get_filename_already_analyzed_file(path.join(data_dir, "to_analyze"))
        five_most_common_words.write.csv(
            path.join(data_dir, "analyzed",  # Save analyzed file in "analyzed" sub-dir
                      analyzed_file.replace(".txt", ".csv")))
        five_most_common_words.show()
    except (ValueError, AnalysisException):
        pass


def get_filename_already_analyzed_file(dir_to_files_to_analyze: str) -> str:
    list_of_files: List[str] = []
    for file_name in listdir(dir_to_files_to_analyze):
        if file_name.endswith(".txt"):
            list_of_files.append(file_name)
    return min(list_of_files, key=path.getctime)
