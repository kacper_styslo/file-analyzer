# PSL
from typing import Tuple

# Third part
from pyspark import SparkContext
from pyspark.sql import SparkSession, SQLContext
from pyspark.streaming import StreamingContext


def create_pyspark_operators() -> Tuple[SparkSession, SparkContext, StreamingContext, SQLContext]:
    """
    This functions creates and returns two pyspark operators.
    :returns: SparkSession, SparkContext, StreamingContext, SQLContext, SparkSession
    """
    spark_session = SparkSession.builder.appName("kstys_apps").getOrCreate()
    spark_context = spark_session.sparkContext
    streaming_context = StreamingContext(spark_context, 1)
    sql_context = SQLContext(spark_context)
    return spark_session, spark_context, streaming_context, sql_context


SPARK_SESSION, SPARK_CONTEXT, SPARK_STREAMING, SQL_CONTEXT = create_pyspark_operators()
